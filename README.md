# TYPO3 bootstrap

This is the base for most TYPO3 projects of [hauptsache.net]
and therefore has some assumptions that are specific to us and our hosting.
However: you might still find it interesting to see a possible way to set up a TYPO3 project.

## Local installation

Run these commands in a folder you want to create your project in.

 - `curl https://bitbucket.org/hauptsachenet/typo3-bootstrap/get/master.tar.gz|tar -xzs /^[a-z0-9\-]*//`
 - `make serve`
 
## Setup a project

This is a checklist of what to do when creating a new project assuming the source will be on [Bitbucket]
and the project will run on [Mittwald]. Other setups are possible but require modification of the deployment process
or (in the case that you don't use Bitbucket) an entirely different deployment configuration altogether. 

- Start by creating a Mittwald project since that takes the longest. Use the software preset (selected by default) and enable ssh.
- Create a domain name using kis.hosteurope.de (you can get the IP from the server before the project is ready)
- Create a bitbucket repository (at best with the same name as the Mittwald project)
- Now follow the installation steps for your local instance above
- Configure the Mittwald user id's in `bitbucket-pipelines.yml` and the `Makefile`
- Be sure to add the newly generated files into your git repository (composer.lock, yarn.lock and LocalConfiguration.php)
- When Mittwald is done, enable pipelines in bitbucket and generate an ssh key. Put the public key on the Mittwald server.
- Create a database and move your database to Mittwald this one time ~
- Create a .env file on the Mittwald server located at `html/{branch-name}` with the content described below.
- Configure your domain in Mittwald to point to `html/{branch-name}/current/web`.
- Add a let's encrypt certificate to the domain.
- You should now be able to deploy your project for the first time but don't forget the next steps
- Add the scheduler cronjob (on Mittwald a custom call with `php_cli` to `html/{branch-name}/vendor/bin/typo3cms` with the parameter `scheduler:run`)
- Ensure that Mittwald performance plus is active

## .env file

Following .env configurations should be in the live enviornment

```bash
DATABASE_URL=mysqli://username:password@host:3306/database
```
g
But there are more you might be interested in
```bash
MAILER_URL=smtp://username:password@smtp.mailtrap.io:2525
IMAGEMAGICK_PATH=/usr/local/bin/
LOG_DEPRECATION=1
PLATFORM=docker
TYPO3_CONTEXT=Development
```

Most of them are used in the `AdditionalConfiguration.php` so look there.

## how to…

### …handle docker and start the project locally

The easiest way is to use the included `makefile`. To see what commands are supplied run `make help`.
Here just a quick overview:

- `make start` starts the project
- `make stop` stops the project
- `make cc` clears all available caches and updates database schemas
- `make pull` gets the current live database and fileadmin (you may need the ssh credentials)
- `make log` shows all logfiles (except php error log because typo3 does not accept stderr as log_file and overrides it)

However, you can use docker directly if you know what you are doing and for debugging.
Services are orchestrated using [docker-compose]. So the `docker-compose` command is your friend.

This projects brings most commands required to run it with it. (besides make and docker)
These commands live within the `bin` folder. Here are the most important ones:

- `bin/composer` runs a dockerized version of composer to handle php dependencies.
- `bin/yarn` runs a dockerized version of yarn to handle css/js dependencies.
- `bin/typo3cms` runs the [helhum/typo3-console] for things like clearing the cache or just generally run commands.

### …organize templates

We use the normal typo3 directory structure meaning templates are organized within

- `Resources/Private/Layouts`
- `Resources/Private/Partials`
- `Resources/Private/Templates`

On top of that we use the [BEM] schema to name and organize templates and partials whenever possible.
This means all styles and scripts are next to their template.

Let's say you want to create a partial `FancyHeadline.html`
Your directory structure would look like this:

- `Resources/Private/Templates/Partials/FancyHeadline.html`
- `Resources/Private/Templates/Partials/FancyHeadline.js`
- `Resources/Private/Templates/Partials/FancyHeadline.scss`

and the content would look like this:

```html
<div class="fancy-headline">{content}</div>
```

```Javascript
import $ from 'jquery';

// i always recommend this initial loop setup.
// this way your partial is reusable but does not do anything if not used at all.
$('.fancy-headline').each(function () {
    $(this).addClass('fancy-headline--fly-in');    
});
```

```scss
.fancy-headline {
    font-size: 20px;
    transform: translateY(-20px);
    transition: transform .2s;
    
    &.fancy-headline--fly-in {
        transform: translateY(0);
    }
}
```

### …handle resources like css/js

We use webpack to handle css/js output.
This allows us to use powerful post processing tools like [autoprefixer] and [babel]
and it also avoids some issues when deploying projects using the default frontend stack of TYPO3.

What you need to know is that all resources are processed using webpack within a docker service fittingly named `webpack`.
This service runs the webpack-dev-server in the background and serves files though port `:5002` (by default).
More information may be available though the [webpack integration] extension.

If you have issues with css or js files not coming though to the frontend (or missing entirely) you should
look into the webpack logs either though `make log` or `docker-compose logs -f -n100 webpack`.

Webpack is configured using the `webpack.config.js` file in the root of this project.
Most important there are the entrypoints. By default `web/typo3conf/ext/hn_templates/ext_index.js` is included.
If you create further extensions, you'll need to add your extensions as entrypoints too in the order they must be loaded.

This entrypoint file simply includes all scss and js files in the `Resource/Private` folder but you may add more
complex logic if needed.

If you want a production build of all frontend resources simply run `bin/yarn build`.
You can then also look at how heavy your js bundle is by opening the url `/build/report.html`.

[hauptsache.net]: https://www.hauptsache.net/
[Bitbucket]: https://bitbucket.org/
[Mittwald]: https://www.mittwald.de/
[docker-compose]: https://docs.docker.com/compose/
[helhum/typo3-console]: https://github.com/TYPO3-Console/TYPO3-Console
[BEM]: http://getbem.com/introduction/
[autoprefixer]: https://github.com/postcss/autoprefixer
[babel]: https://babeljs.io/
[webpack integration]: https://packagist.org/packages/hn/webpack-integration
