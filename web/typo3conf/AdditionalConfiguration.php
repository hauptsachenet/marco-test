<?php

call_user_func(function () {
    $dotenvFile = dirname(dirname(dirname(__DIR__))) . '/.env';
    if (file_exists($dotenvFile)) {
        $dotenv = new \Symfony\Component\Dotenv\Dotenv();
        $dotenv->populate($dotenv->parse(file_get_contents($dotenvFile), $dotenvFile));
    }

    if (getenv('DATABASE_URL')) {
        $url = parse_url(getenv('DATABASE_URL'));
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['driver'] = strtr($url['scheme'], '-', '_');
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'] = $url['user'] ?? '';
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'] = $url['pass'] ?? '';
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'] = $url['host'] ?? 'localhost';
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['port'] = $url['port'] ?? '';
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'] = substr($url['path'], 1);
        parse_str($url['query'] ?? '', $options);
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'] += $options;
    }

    if (getenv('MAILER_URL')) {
        $url = parse_url(getenv('MAILER_URL'));
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] = $url['scheme'];
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username'] = $url['user'] ?? '';
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_password'] = $url['pass'] ?? '';
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_server'] = $url['host'] ?? '';
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_port'] = $url['port'] ?? '';
        // this is an attempt to make the options sort-of™ compatible with swiftmail's url
        parse_str($url['query'] ?? '', $options);
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_sendmail_command'] = $options['command'] ?? '';
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_encrypt'] = $options['encryption'] ?? '';
    }

    if (getenv('IMAGEMAGICK_PATH')) {
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path'] = getenv('IMAGEMAGICK_PATH');
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path_lzw'] = getenv('IMAGEMAGICK_PATH');
    } else if (preg_match('#^p\d{6}$#', getenv("USER"))) {
        // mittwald specific paths
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path'] = '/usr/local/bin/';
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path_lzw'] = '/usr/local/bin/';
    }

    if (getenv('CLOUDCONVERT_APIKEY')) {
        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['video']['converter'] = 'CloudConvert';
        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['video']['cloudConvertApiKey'] = getenv('CLOUDCONVERT_APIKEY');
    }

    // always set the mysql time_zone equal to the php time_zone
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['setDBinit'] = implode(";\n", array_filter([
        rtrim($GLOBALS['TYPO3_CONF_VARS']['SYS']['setDBinit'] ?? '', ";\n"),
        "SET time_zone = '" . addslashes(date_default_timezone_get()) . "'",
    ]));

    $applicationContext = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext();
    $dev = $applicationContext !== null ? $applicationContext->isDevelopment() : true;

    if ($applicationContext && (string)$applicationContext !== 'Production') {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] .= " ($applicationContext)";
    }

    // set the typical dev settings
    $e_error = E_ERROR | E_RECOVERABLE_ERROR | E_CORE_ERROR | E_COMPILE_ERROR | E_USER_ERROR | E_PARSE;
    $e_warning = E_WARNING | E_CORE_WARNING | E_COMPILE_WARNING | E_USER_WARNING;
    $GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = $dev;
    $GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = $dev;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = $dev ? '*' : '';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = $dev;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = getenv('LOG_DEPRECATION') ? true : false;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['exceptionalErrors'] = $dev ? E_ALL & ~(E_STRICT | E_NOTICE) : $e_error;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = $dev ? 1 : 0;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['belogErrorReporting'] = $e_error | $e_warning;

    // force https login unless this is a docker or development environment
    $GLOBALS['TYPO3_CONF_VARS']['BE']['lockSSL'] = !$dev && getenv('PLATFORM') !== 'docker';
    $GLOBALS['TYPO3_CONF_VARS']['BE']['loginSecurityLevel'] = 'normal';
    $GLOBALS['TYPO3_CONF_VARS']['FE']['loginSecurityLevel'] = 'normal';

    // set own production exception handler which tends more to 404 errors than 5xx
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['productionExceptionHandler'] =
        \Hn\HnTemplates\Handler\ProductionExceptionHandler::class;

    $volatileCache = call_user_func(function () use ($dev) {
        // disable all cache in dev mode
        if ($dev) {
            return \TYPO3\CMS\Core\Cache\Backend\NullBackend::class;
        }

        // if there is apcu use is as it is the fastest cache
        // make sure PATH_site is correctly defined (see condition at the top of the file)
        if (extension_loaded('apcu')) {
            // if the fallback for cli would be file cache, it could not be deleted from the typo3 backend
            return (PHP_SAPI === 'cli' && ini_get('apc.enable_cli') == 0)
                ? \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                : \TYPO3\CMS\Core\Cache\Backend\ApcuBackend::class;
        }

        // Fallback to database isn't possible as the database would have to create new tables
        // This fallback is not perfect: switching between apcu on and off can lead to old cache showing
        return \TYPO3\CMS\Core\Cache\Backend\FileBackend::class;
    });

    // these caches aren't that important and regenerating them isn't the end of the world.
    // therefor disabling them or putting them into an unsafe cache is acceptable.
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_imagesizes']['backend'] = $volatileCache;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['extbase_datamapfactory_datamap']['backend'] = $volatileCache;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['extbase_object']['backend'] = $volatileCache;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['extbase_reflection']['backend'] = $volatileCache;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['l10n']['backend'] = $volatileCache;

    if ($dev) {
        // this is the most annoying cache during development since typoscript is saved there
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_hash']['backend'] =
            \TYPO3\CMS\Core\Cache\Backend\NullBackend::class;
    }
});
