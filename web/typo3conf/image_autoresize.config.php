<?php
return [
    'directories' => 'fileadmin/',
    'file_types' => 'jpg,jpeg,png,ai,bmp,tif,tiff',
    'threshold' => '1000K',
    'max_width' => '2400',
    'max_height' => '1800',
    'max_size' => '10M',
    'auto_orient' => '1',
    'conversion_mapping' => 'ai => jpg,bmp => jpg,pcx => jpg,tga => jpg,tif => jpg,tiff => jpg',
    'keep_metadata' => '1',
    'resize_png_with_alpha' => '1',
];
