<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig("<INCLUDE_TYPOSCRIPT: source=\"FILE:EXT:$_EXTKEY/Configuration/TSconfig/Page/tsconfig.txt\">");
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig("<INCLUDE_TYPOSCRIPT: source=\"FILE:EXT:$_EXTKEY/Configuration/TSconfig/User/tsconfig.txt\">");

\Hn\HnTemplates\Hook\TypoScriptTemplateHook::addStaticInclude("EXT:fluid_styled_content/Configuration/TypoScript/");
\Hn\HnTemplates\Hook\TypoScriptTemplateHook::addStaticInclude("EXT:form/Configuration/TypoScript/");
\Hn\HnTemplates\Hook\TypoScriptTemplateHook::addStaticInclude("EXT:seo/Configuration/TypoScript/XmlSitemap/");
\Hn\HnTemplates\Hook\TypoScriptTemplateHook::addStaticInclude("EXT:$_EXTKEY/Configuration/TypoScript/");

// add hook to create root template
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Core/TypoScript/TemplateService']['runThroughTemplatesPostProcessing'][$_EXTKEY] =
    \Hn\HnTemplates\Hook\TypoScriptTemplateHook::class . '->addTypoScriptTemplate';

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Resource\OnlineMedia\Helpers\YouTubeHelper::class]['className']
    = \Hn\HnTemplates\Override\YouTubeHelper::class;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\DataProcessing\GalleryProcessor::class]['className']
    = \Hn\HnTemplates\Override\GalleryProcessor::class;
