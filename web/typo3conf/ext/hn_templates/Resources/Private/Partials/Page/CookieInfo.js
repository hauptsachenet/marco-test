import $ from 'jquery';

const TAG_MANAGER_CODE = null; // example 'GTM-5VVLMR'

let cookieDefined = document.cookie.match(/cookies-accepted=([^&;]*)/);
if (cookieDefined) {
    decodeURIComponent(cookieDefined[1]).split(',').forEach(includeTag);
} else {
    showCookieInfo();
}

function showCookieInfo() {
    const $cookieInfo = $('.cookie-info');
    $cookieInfo.removeClass('cookie-info--hidden');
    $(document).on('resize', resizeCookieInfo);
    resizeCookieInfo();

    $cookieInfo.on('click', '[type="submit"][name="all"]', function () {
        $cookieInfo.find('input[type="checkbox"]:not(:checked)').prop({checked: true});
    });

    $cookieInfo.on('submit', 'form', function () {
        const concent = $(this).find(':checked').toArray().map(checkbox => checkbox.name);
        const cookieValue = concent.map(encodeURIComponent).join(',');
        document.cookie = 'cookies-accepted=' + cookieValue + '; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';

        $cookieInfo.addClass('cookie-info--hidden');
        $(document).off('resize', resizeCookieInfo);

        concent.forEach(includeTag);
    });

    function resizeCookieInfo() {
        $('body').css({paddingBottom: $cookieInfo.outerHeight()});
    }
}

function includeTag(tagName) {
    console.log('include tag', tagName);
    switch (tagName) {
        case "google_tag_manager":
            if (TAG_MANAGER_CODE !== null) {
                // @formatter:off
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer',TAG_MANAGER_CODE);
                // @formatter:on
            }

            break;
        default:
            console.warn(`unimplemented tag "${tagName}" used.`);
            break;
    }
}
