<?php

$GLOBALS['TCA']['tt_content']['columns']['imagewidth']['config'] = [
    'type' => 'select',
    'renderType' => 'selectSingle',
    'items' => [
        ['Automatic (maximum with no upscale)', 0],
        ['80px', 16 * 5],
        ['160px', 16 * 10],
        ['240px', 16 * 15],
        ['320px', 16 * 20],
        ['Maximum', 9999],
    ]
];
