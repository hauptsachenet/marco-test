<?php

// these fields are unneeded if realurl is in use
unset($GLOBALS['TCA']['pages']['columns']['alias']);
unset($GLOBALS['TCA']['pages']['columns']['target']);

// frontend layout isn't used
unset($GLOBALS['TCA']['pages']['columns']['layout']);
unset($GLOBALS['TCA']['pages']['columns']['newUntil']);
