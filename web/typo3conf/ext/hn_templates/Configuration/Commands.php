<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

$commands = [];
foreach (glob(ExtensionManagementUtility::extPath('hn_templates', 'Classes/Command/*Command.php')) as $file) {
    $shortClassName = basename($file, '.php');
    $camelCommandName = substr($shortClassName, 0, -strlen('Command'));
    $commandName = strtolower(preg_replace('#(?<=[a-z])([A-Z])#', ':\1', $camelCommandName));
    $commands[$commandName] = ['class' => "Hn\\HnTemplates\\Command\\$shortClassName"];
}

return $commands;
