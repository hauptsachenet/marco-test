
# grid-gutter-width and container dependent
styles.content.textmedia.maxW = 690
styles.content.textmedia.maxWInText = 420

# arbitrary
styles.content.textmedia.columnSpacing = 15
styles.content.textmedia.rowSpacing = 15
styles.content.textmedia.borderPadding = 15
styles.content.textmedia.borderWidth = 1
