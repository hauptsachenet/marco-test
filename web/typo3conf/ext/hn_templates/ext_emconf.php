<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'hauptsache.net Base/Template Extension',
    'description' => '',
    'category' => 'misc',
    'state' => 'excludeFromUpdates',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => true,
    'constraints' => [
        'depends' => [],
        'conflicts' => [],
        'suggests' => [],
    ],
    'author' => '',
    'author_email' => '',
    'author_company' => '',
];