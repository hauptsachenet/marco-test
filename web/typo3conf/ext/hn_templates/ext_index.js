import $ from 'jquery';

function requireAll(require) {
    require.keys().forEach(require);
}

requireAll(require.context('./Resources/Private/', true, /^[^_]+\.scss$/));
$(() => requireAll(require.context('./Resources/Private/', true, /^[^_]+\.js$/)));

// load scroll behavior polyfill
if (!("scrollBehavior" in document.documentElement.style)) {
    import("scroll-behavior-polyfill");
}
