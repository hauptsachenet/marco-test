<?php

namespace Hn\HnTemplates\Hook;


use TYPO3\CMS\Core\TimeTracker\TimeTracker;
use TYPO3\CMS\Core\TypoScript\TemplateService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class TypoScriptTemplateHook
{
    private static $staticIncludes = [];

    public static function addStaticInclude(string $path)
    {
        static::$staticIncludes[] = $path;
    }

    public static function getStaticInclude(): array
    {
        return static::$staticIncludes;
    }

    public function addTypoScriptTemplate(array $parameters, TemplateService $parentObject): void
    {
        // No template was found in rootline so far, so a custom "fake" sys_template record is added
        if ($parentObject->outermostRootlineIndexWithTemplate !== 0) {
            return;
        }

        $timeTracker = GeneralUtility::makeInstance(TimeTracker::class);
        $timeTracker->push('create fake template');
        $row = [
            'uid' => 'my_site_template',
            'constants' => '',
            'config' => '',
            'include_static_file' => implode(',', static::$staticIncludes),
            'root' => 1,
            'clear' => 3,
            'nextlevel' => 0,
            'static_file_mode' => 1,
            'title' => 'Root template',
            'sitetitle' => $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'],
        ];
        $parentObject->processTemplate(
            $row,
            "sys_{$row['uid']}",
            $parameters['absoluteRootLine'][0]['uid'],
            "sys_{$row['uid']}"
        );
        $parentObject->rootId = $parameters['absoluteRootLine'][0]['uid'];
        $parentObject->rootLine = array_reverse($parameters['absoluteRootLine']);
        $timeTracker->pull();
    }
}