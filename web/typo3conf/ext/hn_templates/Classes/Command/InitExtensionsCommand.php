<?php

namespace Hn\HnTemplates\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class InitExtensionsCommand extends Command
{
    protected function configure()
    {
        $this->setDescription('Creates the initial extension configuration');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $conf = [
            'scheduler' => [
                'maxLifetime' => '60',
            ],
            'staticfilecache' => [
                'boostMode' => '1',
                'enableGeneratorPlain' => '0',
                'enableGeneratorGzip' => '1',
                'fileTypes' => '',
                'renameTablesToOtherPrefix' => '1',
                'sendCacheControlHeader' => '0',
                'showGenerationSignature' => '0',
                'sendTypo3Headers' => '1',
            ],
        ];

        $configurationService = GeneralUtility::makeInstance(ExtensionConfiguration::class);
        foreach ($conf as $extension => $configuration) {
            foreach ($configuration as $key => $value) {
                $configurationService->set($extension, $key, $value);

                // since #set is not the public api, i use #get (which is part of the public api) to check if it worked
                if ($configurationService->get($extension, $key) !== $value) {
                    $msg = "Failed to set extension configuration $extension/$key:" . json_encode($value);
                    throw new \RuntimeException($msg);
                }

                $output->writeln("Set configuration for <info>$extension/$key</info>: " . json_encode($value));
            }
        }
    }
}
