<?php

namespace Hn\HnTemplates\Command;


use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\OnlineMedia\Helpers\OnlineMediaHelperRegistry;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * This command creates a text page for the text&media element.
 *
 * @SuppressWarnings("CouplingBetweenObjects")
 */
class DemoTextmediaCommand extends Command
{
    private const LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,';

    private const CAPTIONS = [
        'some caption',
        'some very long caption that may go over multiple lines',
        '',
    ];

    protected function configure()
    {
        $this->setDescription("Creates demo pages for the text&media element.");
        $this->addOption('storage', 's', InputOption::VALUE_REQUIRED, "Which storage to use for images", 1);
        $this->addOption('variant', 'c', InputOption::VALUE_REQUIRED, "Some variant presets", "hard");
        $this->addOption('image-motive', 'm', InputOption::VALUE_REQUIRED, "Which lorem pixel image category to use", "nature");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws \TYPO3\CMS\Core\Resource\Exception\ExistingTargetFileNameException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $storageRepository = GeneralUtility::makeInstance(StorageRepository::class);
        $resourceStorage = $storageRepository->findByUid($input->getOption('storage'));
        $motive = $input->getOption('image-motive');
        $files = $this->getFiles($output, $input->getOption('variant'), $resourceStorage, $motive);

        $data = [
            'pages' => [
                'NEW_page' => [
                    'pid' => 1, // assume it is there
                    'title' => 'Text & Media Test page ' . date('Yz-His'),
                    'backend_layout' => 'pagets__Main',
                    'backend_layout_next_level' => 'pagets__Main',
                ],
            ],
        ];

        foreach ($GLOBALS['TCA']['tt_content']['columns']['imageorient']['config']['items'] as $index => list($name, $value)) {
            if ($value === '--div--') {
                continue;
            }

            // those variants are removed by tsconfig
            if (in_array($value, [1, 9])) {
                continue;
            }

            // create just text
            $this->createContentElement($data, $name, $value, []);

            // create a content element with each variant
            foreach ($files as $file) {
                $this->createContentElement($data, $name, $value, [$file]);
            }

            // create a content element with all images attached
            $this->createContentElement($data, $name, $value, $files);
        }

        $output->write("Create records... ");
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        $tce->stripslashes_values = 0;
        $tce->start($data, []);
        $tce->process_datamap();
        $output->writeln("done");
    }

    /**
     * @param OutputInterface $output
     * @param ResourceStorage $resourceStorage
     * @param int $width
     * @param int $height
     * @param string $motive
     *
     * @return FileInterface
     * @throws \TYPO3\CMS\Core\Resource\Exception\ExistingTargetFileNameException
     */
    protected function downloadDemoFile(OutputInterface $output, ResourceStorage $resourceStorage, int $width, int $height, string $motive): FileInterface
    {
        static $counter = 0;
        $httpClient = GeneralUtility::makeInstance(Client::class);

        $output->write("Download demo image... ");
        $file = GeneralUtility::tempnam('lorempixel', '.jpg');
        $httpClient->get("http://lorempixel.com/$width/$height/$motive/" . $counter++, ['sink' => $file]);
        $file = $resourceStorage->addFile($file, $resourceStorage->getDefaultFolder(), date('YmdHis') . '.jpg');
        $output->writeln("done, new uid is {$file->getProperty('uid')}: {$file->getIdentifier()}");

        return $file;
    }

    protected function addYouTubeVideo(OutputInterface $output, ResourceStorage $resourceStorage): FileInterface
    {
        $output->write("Register youtube video... ");
        $file = OnlineMediaHelperRegistry::getInstance()->transformUrlToFile(
            'https://www.youtube.com/watch?v=fLHPIYNtig4',
            $resourceStorage->getDefaultFolder()
        );
        $output->writeln("done, new uid is {$file->getProperty('uid')}: {$file->getIdentifier()}");

        return $file;
    }

    /**
     * @param OutputInterface $output
     * @param $variant
     * @param ResourceStorage $resourceStorage
     * @param $motive
     *
     * @return array
     * @throws \TYPO3\CMS\Core\Resource\Exception\ExistingTargetFileNameException
     */
    private function getFiles(OutputInterface $output, $variant, ResourceStorage $resourceStorage, $motive): array
    {
        switch ($variant) {
            case "hard":
                $files = [
                    $this->downloadDemoFile($output, $resourceStorage, 1920, 1920 / 4 * 3, $motive),
                    $this->downloadDemoFile($output, $resourceStorage, 600, 800, $motive),
                    $this->downloadDemoFile($output, $resourceStorage, 200, 200, $motive),
                    $this->addYouTubeVideo($output, $resourceStorage),
                ];
                break;
            case "simple":
                $files = [
                    $this->downloadDemoFile($output, $resourceStorage, 1920, 1920 / 4 * 3, $motive),
                    $this->downloadDemoFile($output, $resourceStorage, 1920, 1920 / 4 * 3, $motive),
                    $this->downloadDemoFile($output, $resourceStorage, 1920, 1920 / 4 * 3, $motive),
                    $this->downloadDemoFile($output, $resourceStorage, 1920, 1920 / 4 * 3, $motive),
                ];
                break;
            default:
                throw new \RuntimeException("Unknown variant $variant.");
        }
        return $files;
    }

    protected function createContentElement(array &$data, $name, $value, array $files): void
    {
        static $counter = 0;

        /** @var LanguageService $languageService */
        $languageService = $GLOBALS['LANG'];
        $contentId = "NEW" . uniqid();
        $fileIds = [];

        foreach ($files as $i => $file) {
            $fileIds[] = $fileId = "NEW" . uniqid();
            $data['sys_file_reference'][$fileId] = [
                'pid' => 'NEW_page',
                'table_local' => 'sys_file',
                'uid_local' => $file->getUid(),
                'tablenames' => 'tt_content',
                'fieldname' => 'assets',
                'uid_foreign' => $contentId,
                'title' => 'some title',
                'description' => self::CAPTIONS[$counter++ % count(self::CAPTIONS)],
            ];
        }

        $data['tt_content'][$contentId] = [
            'pid' => 'NEW_page',
            'CType' => 'textmedia',
            'header' => $languageService->sL($name),
            'bodytext' => self::LOREM_IPSUM,
            'imageorient' => $value,
            'assets' => implode(',', $fileIds),
            'space_before_class' => 'medium',
            'imagecols' => 2,
        ];
    }
}
