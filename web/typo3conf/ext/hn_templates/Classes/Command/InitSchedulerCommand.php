<?php

namespace Hn\HnTemplates\Command;


use SFC\Staticfilecache\Cache\StaticFileBackend;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Cache\Backend\FileBackend;
use TYPO3\CMS\Core\Cache\Backend\SimpleFileBackend;
use TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Scheduler\Task;
use TYPO3\CMS\Recycler\Task\CleanerTask;
use TYPO3\CMS\Scheduler\Execution;
use TYPO3\CMS\Scheduler\Scheduler;
use TYPO3\CMS\Scheduler\Task\AbstractTask;
use TYPO3\CMS\Scheduler\Task\CachingFrameworkGarbageCollectionTask;
use TYPO3\CMS\Scheduler\Task\RecyclerGarbageCollectionTask;
use TYPO3\CMS\Scheduler\Task\TableGarbageCollectionTask;

/**
 * This command creates the initial scheduler tasks.
 *
 * Warnings for coupling are suppressed because this class is more a configuration than a service.
 * @SuppressWarnings("CouplingBetweenObjects")
 */
class InitSchedulerCommand extends Command
{
    protected function configure()
    {
        $this->setDescription("Creates initial scheduler tasks");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $tasks = [];

        $tasks['remove expired cache entries'] = new CachingFrameworkGarbageCollectionTask();
        $tasks['remove expired cache entries']->selectedBackends = [
            Typo3DatabaseBackend::class,
            FileBackend::class,
            SimpleFileBackend::class,
            StaticFileBackend::class, // garbage collecting static file cache is important: do at least hourly
        ];
        $tasks['remove expired cache entries']->setExecution(static::createExecution('0-55/5 * * * *'));

        $tasks['generic table cleanup'] = new TableGarbageCollectionTask();
        $tasks['generic table cleanup']->allTables = true;
        $tasks['generic table cleanup']->setExecution(static::createExecution('0-55/5 1-7/1 * * 6-7/1'));

        $tasks['fileadmin garbage collection after 90 days'] = new RecyclerGarbageCollectionTask();
        $tasks['fileadmin garbage collection after 90 days']->numberOfDays = 90;
        $tasks['fileadmin garbage collection after 90 days']->setExecution(static::createExecution('0-55/5 1-7/1 * * 6-7/1'));

        $tasks['remove deleted fe_users after 90 days'] = new CleanerTask();
        $tasks['remove deleted fe_users after 90 days']->setPeriod(90);
        $tasks['remove deleted fe_users after 90 days']->setTcaTables(['fe_users']);
        $tasks['remove deleted fe_users after 90 days']->setExecution(static::createExecution('0-55/5 1-7/1 * * 6-7/1'));

        $tasks['boost queue worker'] = new Task();
        $tasks['boost queue worker']->setCommandIdentifier('staticfilecache:cache:runcacheboostqueue');
        $tasks['boost queue worker']->setArguments(['stopProcessingAfter' => 300 - 10]); // for 5 minutes but no overlapping with the next run
        $tasks['boost queue worker']->setExecution(static::createIntervalExecution(300 - 5)); // ~ every 5 minutes + some tolerance

        $tasks['boost queue cleanup'] = new Task();
        $tasks['boost queue cleanup']->setCommandIdentifier('staticfilecache:cache:cleanupcacheboostqueue');
        $tasks['boost queue cleanup']->setExecution(static::createExecution('0-55/5 1-7/1 * * 6-7/1'));

        $scheduler = GeneralUtility::makeInstance(ObjectManager::class)->get(Scheduler::class);
        /** @var AbstractTask $task */
        foreach ($tasks as $name => $task) {
            $escapedTaskClass = addslashes(addslashes(get_class($task)));
            $existingTasks = $scheduler->fetchTasksWithCondition("serialized_task_object LIKE '%$escapedTaskClass%' AND description LIKE '%$name%'");
            if (count($existingTasks) > 0) {
                $output->writeln("<error>There already is a $name (" . get_class($task) . "), skipped</error>");
                continue;
            }

            $task->setDescription($name);
            $task->setTaskGroup(0);
            $scheduler->addTask($task);
            $output->writeln("Created $name (" . get_class($task) . ")");
        }
    }

    private static function createIntervalExecution(int $interval): Execution
    {
        $execution = new Execution();
        $execution->setStart(time());
        $execution->setInterval($interval);
        return $execution;
    }

    private static function createExecution(string $cronCmd = '*/5 * * * *'): Execution
    {
        $execution = new Execution();
        $execution->setStart(time());
        $execution->setCronCmd(preg_replace_callback('#(\d+)-(\d+)/(\d+)#', function ($match) {
            return mt_rand($match[1] / $match[3], $match[2] / $match[3]) * $match[3];
        }, $cronCmd));
        return $execution;
    }
}
