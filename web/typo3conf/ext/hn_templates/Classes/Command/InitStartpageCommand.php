<?php

namespace Hn\HnTemplates\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class InitStartpageCommand extends Command
{
    protected function configure()
    {
        $this->setDescription("Creates the initial page for this site.");
        $this->addOption('force', 'f', InputOption::VALUE_NONE, "");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('force')) {
            $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('pages');
            if ($connection->count('uid', 'pages', []) > 0) {
                $output->writeln("You already have pages, skip demo creation (use --force to override)");
                return 1;
            }
        }

        $data = [
            'pages' => [
                'NEW_1' => [
                    'pid' => 0,
                    'title' => 'Startpage',
                    'hidden' => 0,
                    'is_siteroot' => 1,
                    'backend_layout' => 'pagets__Main',
                    'backend_layout_next_level' => 'pagets__Main',
                ],
                'NEW_2' => [
                    'pid' => 'NEW_1',
                    'title' => 'Privacy policy',
                    'hidden' => 0,
                    'is_siteroot' => 1,
                    'backend_layout' => 'pagets__Main',
                    'backend_layout_next_level' => 'pagets__Main',
                ],
            ],
        ];

        $tce = GeneralUtility::makeInstance(DataHandler::class);
        $tce->stripslashes_values = 0;
        $tce->start($data, []);
        $tce->process_datamap();

        return 0;
    }
}
