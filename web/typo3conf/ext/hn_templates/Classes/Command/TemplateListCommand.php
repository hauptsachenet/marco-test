<?php

namespace Hn\HnTemplates\Command;


use Hn\HnTemplates\Hook\TypoScriptTemplateHook;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TemplateListCommand extends Command
{
    protected function configure()
    {
        $this->setDescription("Show available and enabled templates.");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $table = new Table($output);
        $table->setHeaders(["Title", "Path", "Included"]);
        foreach (self::getTemplates() as $template) {
            $table->addRow($template);
        }
        $table->render();

        $output->writeln("You'll need to flush the cache if you have changed the ext_localconf.php");
    }

    private static function getTemplates(): \Iterator
    {
        $staticInclude = TypoScriptTemplateHook::getStaticInclude();

        foreach ($GLOBALS['TCA']['sys_template']['columns']['include_static_file']['config']['items'] as $item) {
            [$title, $templatePath] = $item;
            $isIncluded = false;

            $staticIncludeIndex = array_search($templatePath, $staticInclude);
            if ($staticIncludeIndex !== false) {
                unset($staticInclude[$staticIncludeIndex]);
                $isIncluded = true;
            }

            yield [$title, $templatePath, $isIncluded ? 'yes' : ''];
        }

        foreach ($staticInclude as $templatePath) {
            yield ['-- not configured --', $templatePath, 'yes'];
        }
    }
}
