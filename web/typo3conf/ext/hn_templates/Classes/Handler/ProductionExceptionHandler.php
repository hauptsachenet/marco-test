<?php

namespace Hn\HnTemplates\Handler;


use TYPO3\CMS\Core\Error\Http\ServiceUnavailableException;
use TYPO3\CMS\Core\Utility\HttpUtility;

class ProductionExceptionHandler extends \TYPO3\CMS\Core\Error\ProductionExceptionHandler
{
    protected function sendStatusHeaders($exception)
    {
        // typo3 page type not found is... not found
        if ($exception instanceof ServiceUnavailableException) {
            if (preg_match('#\[type=\d+\]\[\]#', $exception->getMessage())) {
                header(HttpUtility::HTTP_STATUS_404);
                return;
            }
        }

        // there are probably a lot more exceptions that can occur... add at your own will

        parent::sendStatusHeaders($exception);
    }
}