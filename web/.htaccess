
Options -MultiViews -Indexes
RewriteEngine On

# if the domain is a main tld and has no www. in front of it, redirect to www.
RewriteCond %{HTTP_HOST} ^[^.]+\.(com|org|net|us|co|info|ca|cn|fr|ch|au|in|de|jp|nl|uk|mx|no|ru|br|se|es)$ [NC]
RewriteRule ^ https://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

# redirect to https if this is not the docker environment
RewriteCond %{ENV:PLATFORM} !docker [NC]
RewriteCond %{SERVER_PORT} !443
RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

# always request index.php if the original resource isn't found
FallbackResource /index.php
DirectoryIndex index.php

# typo3 context
SetEnvIfExpr "-z %{ENV:TYPO3_CONTEXT}" TYPO3_CONTEXT=Production/Unknown
SetEnvIfNoCase Host \.hauptsache\.net$|test|alpha|beta|staging TYPO3_CONTEXT=Production/Staging
SetEnvIfNoCase Host ^www\.|^[^.]+\.(com|org|net|us|co|info|ca|cn|fr|ch|au|in|de|jp|nl|uk|mx|no|ru|br|se|es)$ TYPO3_CONTEXT=Production

# protect all extension folders from external access
<If "%{REQUEST_URI} =~ m#^/(typo3conf/ext|typo3/sysext)/[^/]+/(?!Resources/Public|ext_icon\.(png|svg|gif))#">
    Require all denied
</If>

# somtimes, javascript is detected as text/javascript ~which is outdated
AddType application/javascript .js

# add compression to some types
AddOutputFilterByType DEFLATE text/plain
AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE text/csv
AddOutputFilterByType DEFLATE application/xml
AddOutputFilterByType DEFLATE application/rss+xml
AddOutputFilterByType DEFLATE application/atom+xml
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/x-shockwave-flash
AddOutputFilterByType DEFLATE application/json
AddOutputFilterByType DEFLATE image/svg+xml

# set the expire time for all build resources.
# these resources should never change.
# don't add any extension directories to this list as they might get updates with the same filename.
<If "%{REQUEST_URI} =~ m#^/fileadmin/_processed_/|^/typo3temp/assets/#">
    ExpiresActive on
    ExpiresDefault "access plus 1 year"
</If>

# files in these dirs are versioned but might still change. Add a little cache here to prevent excessive access
<If "%{REQUEST_URI} =~ m#^/(typo3conf/ext|typo3/sysext)/[^/]+/Resources/Public#">
    ExpiresActive on
    ExpiresDefault "access plus 30 minutes"
</If>

Header merge Cache-Control 'public' 'expr=%{REQUEST_FILENAME} !~ /\.php$/'
Header always set X-Content-Type-Options 'nosniff'
Header always set X-UA-Compatible 'IE=edge' 'expr=%{CONTENT_TYPE} =~ m#^text/html#'
Header always set X-XSS-Protection '1; mode=block' 'expr=%{CONTENT_TYPE} =~ m#^text/html#'
Header always merge X-Frame-Options 'SAMEORIGIN' 'expr=%{CONTENT_TYPE} =~ m#^text/html#'
Header always set X-Robots-Tag 'noindex, nofollow' "expr=reqenv('TYPO3_CONTEXT') != 'Production'"

# block most search engines if not in production mode
RewriteCond %{ENV:TYPO3_CONTEXT} !^Production$ [NC]
RewriteCond %{HTTP_USER_AGENT} adsbot-google|googlebot|mediapartners-google [NC,OR]
RewriteCond %{HTTP_USER_AGENT} aolbuild [NC,OR]
RewriteCond %{HTTP_USER_AGENT} baidu [NC,OR]
RewriteCond %{HTTP_USER_AGENT} bingbot|bingpreview|msnbot [NC,OR]
RewriteCond %{HTTP_USER_AGENT} duckduckgo [NC,OR]
RewriteCond %{HTTP_USER_AGENT} slurp [NC,OR]
RewriteCond %{HTTP_USER_AGENT} teoma [NC,OR]
RewriteCond %{HTTP_USER_AGENT} yandex [NC]
RewriteRule ^ - [R=403]

# output the production robots.txt if in production mode
RewriteCond %{ENV:TYPO3_CONTEXT} ^Production$ [NC]
RewriteRule ^robots.txt$ robots-production.txt [NC]

# static file cache minimal implementation
# this implementation only supports html but is faster and like 10 times shorter.

# cleanup HOST
RewriteCond %{HTTP_HOST} ^([^:]+)(:[0-9]+)?$
RewriteRule .* - [E=SFC_HOST:%1]
# check redirect
RewriteCond %{REQUEST_METHOD} GET
RewriteCond %{QUERY_STRING} ^$
RewriteCond %{HTTP:Accept-Encoding} gzip [NC]
RewriteCond %{HTTP_COOKIE} !be_typo_user [NC]
RewriteCond %{HTTP_COOKIE} !staticfilecache [NC]
RewriteCond %{DOCUMENT_ROOT}/typo3temp/tx_staticfilecache/%{REQUEST_SCHEME}/%{ENV:SFC_HOST}/%{SERVER_PORT}%{REQUEST_URI}/index.html.gz -f
RewriteRule .* typo3temp/tx_staticfilecache/%{REQUEST_SCHEME}/%{ENV:SFC_HOST}/%{SERVER_PORT}%{REQUEST_URI}/index.html.gz
# make sure that the html.gz files are delivered as gzip but without additional compression
<FilesMatch \.html\.gz$>
    Header append Content-Encoding gzip
    Header merge Vary Accept-Encoding
    SetEnv no-gzip 1
    ForceType text/html
</FilesMatch>
