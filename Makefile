SHELL=/bin/sh
PORT_PREFIX?=50
ADDRESS=http://$(shell hostname):$(PORT_PREFIX)00/

# this command will poll the health of all containers until none are "starting"
WAIT_FOR_DOCKER=while [ "`docker-compose ps|grep 'health: starting'`" ]; do echo "waiting for full startup"; sleep 15; done

.PHONY: help serve start stop install cc log test pull
help:
	$(info -       help: Prints this help text.)
	$(info -      serve: Start all services and opens your browser to show the page (mac only).)
	$(info -      start: Start all services.)
	$(info -       stop: Stop all services.)
	$(info -    install: Installs all dependencies of the project.)
	$(info -         cc: Clears all caches and updates the database schema)
	$(info -        log: Show (and follow) the log files.)
	$(info -       test: Run all tests for this project)
	$(info -       lint: Run static code analysis)
	$(info -       pull: Syncs the local instance with the remote instance)

serve: start
	open $(ADDRESS)

start: install web/build
	$(info the project will be running under the address $(ADDRESS))
	docker-compose up --detach
	$(WAIT_FOR_DOCKER)

stop:
	docker-compose down --remove-orphans

install: web/typo3conf/LocalConfiguration.php vendor node_modules docker-compose.log web/fileadmin

cc: install
	docker-compose up --detach php
	$(WAIT_FOR_DOCKER)
	-bin/typo3cms cache:flush --force
	bin/typo3cms database:updateschema --verbose
	bin/typo3cms extension:setupactive

log:
	docker-compose logs --tail=$(if $(TAIL),$(TAIL),4) --follow

test: install
	docker-compose up --detach php
	$(WAIT_FOR_DOCKER)
	bin/phpunit -c phpunit.unit.xml
	bin/phpunit -c phpunit.functional.xml

lint: node_modules
	bin/phpmd web/typo3conf/ext/hn_*/Classes/ text codesize,design
	bin/yarn stylelint web/typo3conf/ext/hn_*/Resources/Private/**/*.scss
	bin/security-checker security:check

pull: REMOTE_SSH_SOCKET=/tmp/ssh_$(notdir $(CURDIR))
pull: REMOTE_HOST=p506033@p506033.mittwaldserver.info
pull: REMOTE_DIR=html/master/current
pull: REMOTE_PHP=php_cli
pull: EXPORT_DB_CMD=$(REMOTE_PHP) $(REMOTE_DIR)/vendor/bin/typo3cms database:export --verbose --connection=Default  --exclude "cf_*" --exclude "cache_*" --exclude="*_sessions" --exclude="sys_domain" --exclude="sys_file_processedfile" --exclude="sys_log" --exclude="sys_history"
pull: SSH_CMD=ssh -o ControlPath=$(REMOTE_SSH_SOCKET)
pull: start
	rm -f $(REMOTE_SSH_SOCKET)
	ssh -nNf -o ControlMaster=yes -o ControlPath=$(REMOTE_SSH_SOCKET) -o ControlPersist=3600 $(REMOTE_HOST)
	$(SSH_CMD) - '$(EXPORT_DB_CMD) |gzip --fast' |gunzip |docker exec -i `docker-compose ps -q db` mysql -uroot -ppassword database
	-bin/typo3cms cache:flush --force
	bin/typo3cms database:updateschema --verbose
	bin/typo3cms extension:setupactive
	rsync -rtve '$(SSH_CMD)' --delete --max-size=10m --exclude="_*_/" :$(REMOTE_DIR)/web/fileadmin/ web/fileadmin/
	ssh -O exit -o ControlPath=$(REMOTE_SSH_SOCKET) -

###################################################
## starting here, every target is an actual target

# install typo3 if the local configuration is missing
web/typo3conf/LocalConfiguration.php: | vendor docker-compose.log
	docker-compose up --detach php
	$(WAIT_FOR_DOCKER)
	bin/typo3cms install:setup --site-setup-type=no --web-server-config=none
	bin/typo3cms init:extensions
	bin/typo3cms database:updateschema --verbose
	bin/typo3cms init:scheduler
	-bin/typo3cms init:startpage

vendor: $(wildcard composer.*) | docker-compose.log
	bin/composer install

node_modules: package.json $(wildcard yarn.lock) | docker-compose.log
	bin/yarn install
	touch node_modules

# restart webpack if config files change
web/build: webpack.config.js .browserslistrc node_modules
	docker-compose stop webpack

web/fileadmin:
	mkdir -p web/fileadmin

# this is somewhat a hack. (although documented: https://www.gnu.org/software/make/manual/html_node/Empty-Targets.html#Empty-Targets)
# The docker build process does not generate a file accessable to me
# but since i don't want to build every time, i touch a "log" file that will keep the mod time.
docker-compose.log: $(wildcard Dockerfile*)
	docker-compose build
	touch docker-compose.log
