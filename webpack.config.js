const webpack = require('webpack');
const path = require("path");

const CleanWebpackPlugin = require('clean-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const TerserPlugin = require("terser-webpack-plugin");
const CompressionPlugin = require('compression-webpack-plugin');
const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');

module.exports = (env, argv) => ({
    entry: {
        index: [
            './web/typo3conf/ext/hn_templates/ext_index.js',
        ],
    },
    output: {
        path: path.resolve(__dirname, 'web/build'),
        filename: argv.mode === 'development' ? '[name].js' : '[contenthash].js',
        publicPath: argv.host ? `http://${argv.host}:${argv.port || 8080}/build/` : '/build/',
    },
    // cheap-source-map's are necessary for this to work with mini-css-extract-plugin
    devtool: argv.mode === 'development' ? 'cheap-source-map' : 'none',
    devServer: {
        headers: {'Access-Control-Allow-Origin': '*'},
        overlay: {warnings: false, errors: true},
    },
    watchOptions: {
        ignored: /node_modules/,
    },
    module: {
        rules: [
            {
                test: /\.js$/i,
                exclude: /node_modules|bower_components/,
                loader: 'babel-loader',
                options: {
                    plugins: [
                        "@babel/plugin-syntax-dynamic-import",
                        ...(argv.mode === 'development' ? [] : [
                            ["transform-remove-console", {exclude: ['error', 'warn']}],
                        ]),
                    ],
                    presets: [['@babel/preset-env', {
                        useBuiltIns: 'usage', // useBuiltIns will add appropriate language polyfills on usage
                        debug: Boolean(argv.debug), // the info what transforms are applied does not hurt
                    }]],
                },
            },
            {
                test: /\.scss$|\.css$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {hmr: argv.hot},
                    },
                    {
                        loader: 'css-loader',
                        options: {importLoaders: 1},
                    },
                    {
                        // IF REMOVED: also remove the importLoaders option in css-loader
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            plugins: [
                                require('autoprefixer')({remove: false}),
                                require('cssnano')({preset: ['default', {normalizeWhitespace: argv.mode !== 'development'}]}),
                            ],
                        },
                    },
                ],
            },
            {
                test: /\.scss$/i,
                loader: 'sass-loader',
            },
            {
                test: /\.(png|jpg|jpeg|gif|ico|svg|webp|woff|woff2|ttf|eot|otf)$/i,
                loader: 'url-loader',
                options: {
                    fallback: 'file-loader',
                    limit: 8192,
                },
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(
            'web/build',
            {exclude: ['.htaccess']},
        ),
        new ManifestPlugin({
            basePath: 'build/',
            writeToFileEmit: true,
        }),
        new StyleLintPlugin({
            context: path.resolve(__dirname, 'web/typo3conf/ext/'),
            files: 'hn_*/Resources/Private/**/*.scss',
            lintDirtyModulesOnly: argv.mode === 'development',
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
        }),
        new MiniCssExtractPlugin({
            filename: argv.mode === 'development' ? '[name].css' : '[contenthash].css',
            chunkFilename: argv.mode === 'development' ? '[id].css' : '[contenthash].css',
        }),
    ],
    optimization: {
        minimizer: [
            new TerserPlugin(),
            new CompressionPlugin({
                test: /\.(js|css|svg|ttf)$/,
                filename: '[path].br[query]',
                algorithm: 'brotliCompress',
                compressionOptions: {level: 11},
            }),
            new CompressionPlugin({
                test: /\.(js|css|svg|ttf)$/,
                filename: '[path].gz[query]',
                algorithm: 'gzip',
                compressionOptions: {level: 9},
            }),
            new BundleAnalyzerPlugin({
                analyzerMode: 'static',
            }),
        ],
    },
});
