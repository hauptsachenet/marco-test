# note: typo3 8 has an issue with libpcre3 under debian jessie
# php 7.2 is the first image that comes with debian stretch which resolves this issue
# if you need an older php, you'll need to pin libpcre from stretch
FROM chialab/php:7.2-apache

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ffmpeg \
    imagemagick \
 && rm -r /var/lib/apt/lists/*

RUN sed -i 's#/var/www/html#\${DOCUMENT_ROOT}#g' /etc/apache2/sites-enabled/000-default.conf \
 && a2enmod alias deflate expires headers rewrite \
 && echo 'date.timezone=Europe/Berlin' >> /usr/local/etc/php/conf.d/php.ini \
 && echo 'max_execution_time=240' >> /usr/local/etc/php/conf.d/php.ini \
 && echo 'max_input_vars=1500' >> /usr/local/etc/php/conf.d/php.ini \
 && echo 'upload_max_filesize=32M' >> /usr/local/etc/php/conf.d/php.ini \
 && echo 'post_max_size=32M' >> /usr/local/etc/php/conf.d/php.ini \
 && echo 'opcache.enable_file_override=On' >> /usr/local/etc/php/conf.d/php.ini \
 && echo 'opcache.revalidate_freq=0' >> /usr/local/etc/php/conf.d/php.ini

